# Spambase

Wykrywanie spamu za pomocą algorytmów uczenia maszynowego.
Klasyczny problem klasyfikacji maili jako spam lub nie-spam. W załączonych materiałach
dołączyliśmy katalog spambase (zawiera dane w formacie csv oraz opis atrybutów, specyfikację
całej bazy). W pliku spambase.data każdy rekord to opis jednego maila za pomocą 57 atrybutów,
a ostatnim 58 polem w rekordzie jest klasa (1-spam; 0-non-spam). Używając
dowolnego języka (Java, C#, R, Python lub środowiska jak Weka, RapidMiner itp.) należy
zbudować model klasyfikatora binarnego do klasyfikacji spam/nie-spam, oraz zbadać jego
dokładność korzystając z metodyki ewaluacji 10-fold-cross-validation.

---

Prosty klasyfikator oparty na drzewie decyzyjnym został zaimplementowany przy pomocy środowiska **RapidMiner**.

Dokładność klasyfikatora została zbadana korzystając z metodyki **10-fold-cross-validation**.

|Dokładność|
|:--------:|
|`91.59% ` |

---

![process1](https://bytebucket.org/MinioMinioMinio/zadaniatestowe/raw/e07d310339b9c2403d07c77abfa760f937012e35/Spam/process1.png)

![process2](https://bytebucket.org/MinioMinioMinio/zadaniatestowe/raw/e07d310339b9c2403d07c77abfa760f937012e35/Spam/process2.png)

![results](https://bytebucket.org/MinioMinioMinio/zadaniatestowe/raw/e07d310339b9c2403d07c77abfa760f937012e35/Spam/results.png)
