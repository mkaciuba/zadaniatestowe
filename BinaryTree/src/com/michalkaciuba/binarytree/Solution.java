package com.michalkaciuba.binarytree;

public class Solution {
	public int solution(Tree root, int id) {
		return findLevel(root, id, 0);
	}

	private int findLevel(Tree root, int id, int level){
		if(root == null){
			return -1;
		}
		if(root.id != id){
			int res = findLevel(root.l, id, level + 1);
			if(res == -1){
				res = findLevel(root.r, id, level + 1);
			}
			return res;
		} else {
			return level;
		}
	}

}
