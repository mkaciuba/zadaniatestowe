package com.michalkaciuba.binarytree;

public class Main {
	public static void main(String[] args) {
		Tree[] t = new Tree[9];

		for (int i = 0; i < t.length; i++) {
			t[i] = new Tree();
			t[i].id = i;
		}

		t[0].l = t[1];
		t[0].r = t[2];
		t[1].l = t[3];
		t[1].r = t[4];
		t[2].r = t[5];
		t[4].l = t[6];
		t[4].r = t[7];
		t[5].l = t[8];

		Solution s = new Solution();

		for (int i = 0; i < t.length; i++) {
			System.out.println(s.solution(t[0], i));
		}
	}
}
