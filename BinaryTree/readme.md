# Drzewa binarne
Dane jest drzewo binarne składające się z N wierzchołków. Mówimy, że wierzchołek T jest lewym
potomkiem (lub odwrotnie, prawym), jeżeli wartość atrybutu l (lub odwrotnie, r) jest wartością
różną od null.

Mając podaną następującą deklarację:

```java
class Tree{
  public int id;
  public Tree l;
  public Tree r;
}
```
napisz funkcję w klasie Solution:
```java
class Solution {
  public int solution(Tree root, int id);
}
```
która zwróci numer poziomu drzewa, na którym znajduje się wskazany węzeł. Poziomy
ponumerowane są od zera, licząc od korzenia drzewa.

---

Rozwiązanie wykorzystuje algorytm **DFS**.
