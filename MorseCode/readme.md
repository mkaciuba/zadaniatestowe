# Kod Morse'a

Napisz program, który będzie potrafił zakodować tekst w postaci kodu Morse'a oraz odkodować
ciąg kropek i kresek do tekstu. Białe znaki nie muszą być brane pod uwagę przy konwersji.

#### Przykłady:
```
In: ALAMAKOTA
Out: .- .-.. .- -- .- -.- --- - .-
In: .-.. ..- -... .. . .--. .-. --- --. .-. .- -- --- .-- .- -.-.
Out: LUBIEPROGRAMOWAC
```
---

## Rozwiązanie

Zadanie zostało rozwiązane w oparciu o [ten artykuł](https://en.wikipedia.org/wiki/Morse_code) na Wikipedii.

#### Dodatkowe reguły
- Koder akceptuje małe i wielkie litery alfabetu łacińskiego, spację oraz cyfry. Pomija wszystkie pozostałe znaki.
- odstępy pomiędzy wyrazami w kodowanej wiadomości zostają zamienione na **7 spacji** w wiadomości zakodowanej
- Przy rozkodowywaniu każdy odstęp długości 1-6 jest traktowany jako odstęp pomiędzy znakami. Odstęp długości 7 jest zamieniany na spację.


#### Przykłady
```
encode("Wiadomosc TESTOWA 123") -> "· − −  · ·  · − − · ·  − − −  − −  − − −  · · ·  − · − ·        −  ·  · · ·  −  − − −  · − −  · −       · − − − −  · · − − −  · · · − − "

decode("· − −  · ·  · − − · ·  − − −  − −  − − −  · · ·  − · − ·        −  ·  · · ·  −  − − −  · − −  · −       · − − − −  · · − − −  · · · − − ") -> "WIADOMOSC TESTOWA 123"
```
