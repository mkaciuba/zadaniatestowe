package morsecode;

import com.michalkaciuba.morsecode.MorseCode;
import org.junit.Assert;
import org.junit.Test;

public class MorseCodeTest {

	@Test
	public void test(){
		Assert.assertEquals(MorseCode.encode("Wiadomosc testowa 123"), "· − −  · ·  · − − · ·  − − −  − −  − − −  · · ·  − · − ·        −  ·  · · ·  −  − − −  · − −  · −       · − − − −  · · − − −  · · · − − ");
		Assert.assertEquals(MorseCode.decode("· − −  · ·  · − − · ·  − − −  − −  − − −  · · ·  − · − ·        −  ·  · · ·  −  − − −  · − −  · −       · − − − −  · · − − −  · · · − − "), "Wiadomosc testowa 123".toUpperCase());
		Assert.assertEquals(MorseCode.decode(MorseCode.encode("Wiadomosc testowa 123")),"Wiadomosc testowa 123".toUpperCase());
		Assert.assertEquals(MorseCode.encode(""),"");
		Assert.assertEquals(MorseCode.decode(""),"");
	}
}
