package com.michalkaciuba.morsecode;

import java.util.Arrays;

public class MorseCode {
	private final static String[] codes = {
			"     ",
			"− − − − − ",
			"· − − − − ",
			"· · − − − ",
			"· · · − − ",
			"· · · · − ",
			"· · · · · ",
			"− · · · · ",
			"− − · · · ",
			"− − − · · ",
			"− − − − · ",
			"· −",
			"− · · ·",
			"− · − · ",
			"− · · ",
			"· ",
			"· · − · ",
			"− − · ",
			"· · · · ",
			"· · ",
			"· − − − ",
			"− · − ",
			"· − · · ",
			"− − ",
			"− · ",
			"− − − ",
			"· − − · ",
			"− − · − ",
			"· − · ",
			"· · · ",
			"− ",
			"· · − ",
			"· · · − ",
			"· − − ",
			"− · · − ",
			"− · − − ",
			"− − · · "
	};
	private final static char[] symbols = {
			' ',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F',
			'G',
			'H',
			'I',
			'J',
			'K',
			'L',
			'M',
			'N',
			'O',
			'P',
			'Q',
			'R',
			'S',
			'T',
			'U',
			'V',
			'W',
			'X',
			'Y',
			'Z'
	};

	public static String encode(String msg) {
		StringBuilder encMsg = new StringBuilder();
		for (char c : msg.toCharArray()) {
			int idx = Arrays.binarySearch(symbols, Character.toUpperCase(c));
			if (idx != -1) {
				encMsg.append(codes[idx]).append(" ");
			}
		}
		if (encMsg.length() > 0) {
			encMsg.setLength(encMsg.length() - 1);
		}
		return encMsg.toString();
	}

	public static String decode(String encMsg) {
		String[] encSyms = encMsg.split(" ");
		int cnt = 0;
		StringBuilder msg = new StringBuilder();
		for (String encSym : encSyms) {
			if (encSym.isEmpty()) {
				if (++cnt == 6) {
					cnt = 0;
					msg.append(' ');
				}
				continue;
			}
			cnt = 0;
			int idx = indexOf(codes, encSym);
			msg.append(symbols[idx]);
		}
		return msg.toString();
	}

	private static int indexOf(String[] arr, String elem) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i].equals(elem)) {
				return i;
			}
		}
		return -1;
	}


}
