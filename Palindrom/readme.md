# Palindrom

Palindrom to liczba którą czyta się w taki sam sposób od przodu i od tyłu. Napisz program który
wypisze wszystkie palindromy powstałe z pomnożenia przez siebie liczb dwucyfrowych (10-99).

#### Przykłady palindromów:

`9009 = 91 * 99`

`2552 = 44 * 58`

## Wynik

```
121
242
252
272
323
363
414
434
444
464
484
494
525
555
575
585
595
616
636
646
656
666
676
686
696
737
767
777
828
848
858
868
888
949
969
979
989
999
1001
1221
1551
1771
1881
2002
2112
2332
2442
2552
2772
2992
3003
3663
3773
4004
4224
4554
4664
4774
4884
5005
5115
5225
5335
5445
5775
6006
6336
6776
7007
7227
8008
8118
8448
9009
```
