package com.michalkaciuba.palindrom;

import java.util.HashSet;
import java.util.Set;

public class Palindrom {
	public static void printPalindroms() {
		Set<Integer> palindroms = new HashSet<>();
		for (int a = 10; a <= 99; a++) {
			for (int b = a; b <= 99; b++) {
				int prod = a * b;
				if (isPalindrom(prod)) {
					palindroms.add(prod);
				}
			}
		}
		palindroms.stream().sorted().forEach(System.out::println);
	}

	private static boolean isPalindrom(int n) {
		String txt = "" + n;
		int len = txt.length();
		for (int i = 0; i < len / 2; i++) {
			if (txt.charAt(i) != txt.charAt(len - i - 1)) {
				return false;
			}
		}
		return true;
	}
}
